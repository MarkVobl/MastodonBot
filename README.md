# Mastodon User Graph Generator

This script generates a user graph on Mastodon social network using the Mastodon API. It creates Markdown files for each user, containing their profile information, followers, following, and links to their recent posts.

## Prerequisites

To run this script, you need to have the following packages installed:

- `mastodon`: Python library for Mastodon API
- `pytz`: Python library for timezone calculations
- `beautifulsoup4`: Python library for HTML parsing
- `json`: Python library for JSON manipulation

You can install the required packages using `pip`:

```
pip install Mastodon.py pytz beautifulsoup4
```

## Configuration

Before running the script, you need to set up the following variables:

- `Inst`: The base URL of your Mastodon instance.
- `access_token`: Your Mastodon access token.
- `PassTokens`: A list of tokens to filter out users based on their profile notes.
- `FirtsUsers`: A list of usernames (including instance) to start the graph generation from.
- `TemplateUser`: The template for the user's Markdown file.
- `ListUsers`: A dictionary to store the hash of each user's Markdown file.
- `MaxDeep`: Parsing depth
## Usage

To generate the user graph, run the script using a Python interpreter:

```
python Scaner.py
```

The script will start generating user profiles and their connections based on the starting users provided in the `FirtsUsers` list. The graph generation process continues up to a maximum depth specified in the `MaxDeep` variable.

The generated Markdown files for each user will be saved in the `ListUsers` directory. The script also saves the hash of each user's Markdown file in the `ListCreatFiles.json` file for future reference.

## Limitations

- The script only generates a user graph based on the specified starting users. It does not support dynamically expanding the graph based on discovered connections.
- The script only considers users who have posted within the last 30 days and have been active within the last 7 days.
- The graph generation process has a maximum depth limit to prevent an infinite loop.