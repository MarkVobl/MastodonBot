from mastodon import Mastodon
import datetime
from pytz import timezone
from bs4 import BeautifulSoup
import re
import hashlib
import json
import sys

import my_token

Inst="https://mastodon.social/"

access_token = my_token.token # YourToken "bd7zkUHk5p0FJoapENetzEn14elM6vFnGSHMQ8BFDfL"
PassTokens=["#nobot","#nobot_markvobl","#noarchive"]
FirtsUsers = []
MaxDeep=2

m = Mastodon(access_token=access_token, api_base_url=Inst)
me=m.me()

TemplateUser=""
with open("TemplateUser.md","r",encoding="utf-8") as f:
    TemplateUser=f.read()

ListUsers={}
with open('ListCreatFiles.json',"r",encoding="utf8") as json_file:
    ListUsers = json.load(json_file)

#IDFilesList={}
#with open('ListAllFiles.json',"r",encoding="utf8") as json_file:
#    IDFilesList = json.load(json_file)
now = datetime.datetime.now()
months_ago = datetime.datetime.now(timezone('Asia/Yekaterinburg')) - datetime.timedelta(days=30*12)
week_month_ago = now - datetime.timedelta(days=30)
def CreateListTags(User):
    tags = m.account_featured_tags(id=User['id'])
    tags_note = re.findall(r'#\w+', User['note'])
    ListTags=[]
    if User["bot"]:
        ListTags.append(f"#bot : profile\n")
    for tag in tags:
        ListTags.append(f"#{tag['name']} : {tag['statuses_count']}\n")
    for tag in tags_note:
        ListTags.append(f"{tag} : note\n")
    return "".join(ListTags)




def CreateListFollow(User):
    account_id = User['id']
    followers=m.account_followers(id=account_id)
    following=m.account_following(id=account_id)
    ListFollowers=[]
    ListFollowing=[]
    for follower in followers:
        ListFollowers.append(f"[[{follower['username']}_{follower['id']}.md]]\n")
    for follower in following:
        ListFollowing.append(f"[[{follower['username']}_{follower['id']}.md]]\n")
    return ["".join(ListFollowers),"".join(ListFollowing),followers+following]

def HTML_to_Text(HTML):
    soup = BeautifulSoup(HTML, 'html.parser')
    Text = soup.get_text()
    return Text

def CreatePage(User,TemplateUser,Deep):
    account_id = User['id']
    username=User['username']
    created_at=User['created_at']
    note=User['note']
    url=f"{Inst}@{User['acct']}"
    
    avatar=User['avatar']
    statuses = m.account_statuses(account_id,exclude_replies=True)
    links = []
    posts = ""
    i=1
    for status in statuses:
        # Используйте регулярное выражение для поиска ссылок в тексте поста
        status['content']=HTML_to_Text(status['content'])
        if len(status['content'])>0:
            urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', status['content'])
            links.extend(urls)
            posts=posts+f"{i}. {status['content']}\n"
            i=i+1
    ListFollowers,ListFollowing,Follow=CreateListFollow(User)
    tags = CreateListTags(User)
    return f"Deep:{ Deep}\n"+TemplateUser.format(account_id=account_id,username=username,
                               created_at=created_at,note=note,
                               url=url,avatar=avatar,tags=tags,
                               following=ListFollowing,followers=ListFollowers,posts=posts,urls="\n".join(links)),Follow 

def FilterUsers(User):
    if (User["created_at"]<=months_ago) and not(User["last_status_at"] is None):
        if (User["last_status_at"]>=week_month_ago):
            return 1   
    else:
        return 0
def FilterUsers2(User):
    return 1

def PassTokensChek(Text):
    for token in PassTokens:
        if token in Text:
            return 0
    else:
        return 1

def PreparingUserList(UserList):
    global ListUsers
    UserList2=[]
    for User in UserList:
        id=str(User['id'])
        User['note']=HTML_to_Text(User['note'])
        if PassTokensChek(User['note'].lower()):
            if (id not in ListUsers): #or (id in ListUsers and id not in IDFilesList):
                User['id']=id
                UserList2.append(User)
    return UserList2
def WriteList():
    global ListUsers
    json_object = json.dumps(ListUsers, indent = 4) 
    with open('ListCreatFiles.json',"w",encoding="utf8") as f:
        f.write(json_object)

def CreateGraphe(StartUsers,Deep,MaxDeep=3):
    global ListUsers
    StartUsers=PreparingUserList(StartUsers)
    
    if Deep<=MaxDeep and len(StartUsers)>0:
        NewFollowList=[]
        for User in StartUsers:
            id=User['id']
            Page,FollowList=CreatePage(User,TemplateUser,Deep)

            with open(f"../ListUsers/{User['username']}_{id}.md","w",encoding="utf-8") as f:
                f.write(Page)
                
            ListUsers[id]=[hashlib.md5(Page.encode()).hexdigest(),Deep]
            
            CleanFollowList=list(filter(FilterUsers2,FollowList))
            for User2 in CleanFollowList:
                if (User2 not in NewFollowList):
                    NewFollowList.append(User2)
                #IDFilesList[User2["id"]]=0
            print(f"Create {User['username']}_{id} Deep{Deep}: New {len(CleanFollowList)} Deep{Deep+1}")
        
        WriteList()
        CreateGraphe(NewFollowList,Deep+1,MaxDeep)
            
    
def main():
    FirtsUsers=[]
    argvs=sys.argv[1:]
    if(len(argvs)>0):
        for argv in argvs:
            if("@" in argv):
                FirtsUsers.append(m.account_lookup(argv))
        else:
            if(len(FirtsUsers)==0):
                FirtsUsers=[me]
        if argv[-1].isdigit():
            if 0<=int(argv[-1])<10:
                Deep=argv[-1]
    else:
        FirtsUsers=[me]
    
    #print(m.account_lookup("@MarkVobl@mastodon.social"))
    
    CreateGraphe(FirtsUsers,0,MaxDeep)

if __name__ == "__main__":
    #main()
    u=PreparingUserList([m.account_lookup("@bano@mastodon.ml"),m.account_lookup("@MarkVobl@mastodon.social")])
    for i in u:
        print(i)
        print("-"*100)

